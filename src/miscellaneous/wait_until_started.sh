#!/bin/bash
CA_FILE=${CA_FILE:-/elasticsearch/config/searchguard/ssl/ca/root-ca.crt}


if [[ "${NODE_MASTER}" = "true" ]]; then
    RET=1

    while [[ RET -ne 0 ]]; do
        echo "Stalling for Elasticsearch..."
        curl -XGET -k -u "elastic:$$ELASTIC_PWD" "https://es-data-node1:9200/" >/dev/null 2>&1
        RET=$?
        sleep 5
    done
else
    while [ ! -f $CA_FILE ]; do
        sleep 5
    done
fi
